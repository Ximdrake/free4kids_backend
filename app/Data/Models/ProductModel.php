<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;


class ProductModel extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'id';
    protected $table = 'messages';
    // protected $appends = [
    //    'value','name'
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'image_url', 'location_lat','location_long','messages','approval_id','sender','reciever','user_id'
    ];

    public function Users()
    {
        return $this->hasMany('\App\Data\Models\Users', 'user_id', 'id');
    }
}
