<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;


class ReservationModel extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'id';
    protected $table = 'reservation';
    // protected $appends = [
    //    'value','name'
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id','undo','redo','user_id'
    ];

   
}
