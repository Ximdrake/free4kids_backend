<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;

class Users extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'id';
    protected $table = 'users';
    // protected $appends = [
    //    'value','name'
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'reports'
    ];

    // protected $searchable = [
    //     'user_info.firstname',
    //     'user_info.middlename',
    //     'user_info.lastname',
    //     'id',
    //     'uid',
    //     'email', 
    //     'password', 
    //     'access_id',
    //     'loginFlag',
    //     'company_id'
    // ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
    public function reports()
    {
        return $this->hasMany('\App\Data\Models\ReportsModel', 'user_id', 'id');
    }

    public function reviews()
    {
        return $this->hasMany('\App\Data\Models\ReviewsModel', 'user_id', 'id');
    }

    public function location()
    {
        return $this->hasMany('\App\Data\Models\Location', 'user_id', 'id');
    }

    public function products()
    {
        return $this->hasMany('\App\Data\Models\Product', 'user_id', 'id');
    }

    public function reservation()
    {
        return $this->hasMany('\App\Data\Models\ReservationModel', 'user_id', 'id');
    }

    public function approval()
    {
        return $this->hasMany('\App\Data\Models\ApprovalModel', 'user_id', 'id');
    }

    public function messages()
    {
        return $this->hasMany('\App\Data\Models\MessagesModel', 'user_id', 'id');
    }
    // public function user_info() {
    //     return $this->hasOne('\App\Data\Models\UserInfo', 'id', 'uid');
    // }

    // public function user_logs() {
    //     return $this->hasMany('\App\Data\Models\ActionLogs', 'user_id', 'id');
    // }
    
    // public function accesslevel(){
    //     return $this->hasOne('\App\Data\Models\AccessLevel', 'id', 'access_id');
    // }

    // public function position(){
    //     return $this->belongsTo('\App\Data\Models\AccessLevel', 'access_id', 'id');
    // }

    // public function userdata() {
    //     return $this->belongsTo('\App\Data\Models\UserInfo', 'uid', 'id');
    // }

    // public function accesslevelhierarchy(){
    //     return $this->hasOne('\App\Data\Models\AccessLevelHierarchy', 'child_id', 'id');
    // }

    // public function getNameAttribute(){
    //     $name = null;
    //     if(isset($this->user_info)){
    //         $name = $this->user_info->full_name;
    //     }
        
    //     return $name;
    // }

    // public function getValueAttribute(){
    //     $name = null;
    //     if(isset($this->user_info)){
    //         $name = $this->user_info->id;
    //     }
        
    //     return $name;
    // }
}
