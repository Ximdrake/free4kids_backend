<?php

namespace App\Data\Repositories;

use App\Data\Models\MessagesModel;
use App\Data\Repositories\BaseRepository;
use App\Message;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class MessagesRepository extends BaseRepository
{

    protected $messages;

    public function __construct(MessagesModel $messages) 
    {
        $this->messages = $messages;
    }

    public function fetchMessages($data = [])
    {
        $meta_index = "messages";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "messages";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->messages);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->messagess->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function create($data = [])
    {
        // data validation
        
     
        if (!isset($data['user_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "user id is not set.",
            ]);
        }

        if (!isset($data['location_lat'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "location_lat is not set.",
            ]);
        }
        
        if (!isset($data['location_long'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "location_long is not set.",
            ]);
        }

        if (!isset($data['sender'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "sender is not set.",
            ]);
        }
        
        if (!isset($data['reciever'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "reciever is not set.",
            ]);
        }
        if (!isset($data['messages'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "messages is not set.",
            ]);
        }
        if (!isset($data['approval_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "approval_id is not set.",
            ]);
        }
        if (!isset($data['image'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "image is not set.",
            ]);
        }else{
            $data['image_url']=null;      
                request()->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg|max:10000',
        
                ]);
                $imageName = time().'.'.request()->image->getClientOriginalExtension();
                define('UPLOAD_DIR', 'storage/images/');
                $file =  request()->image->move(UPLOAD_DIR,$imageName);
                $url= asset($file);
                $data['image_url'] = $url;        
        }

       
            $messages = $this->messages->init($this->messages->pullFillable($data));
            $messages->save($data);

            if (!$messages->save($data)) {
                return $this->setResponse([
                    "code"        => 500,
                    "title"       => "Data Validation Error.",
                    "description" => "An error was detected on one of the inputted data.",
                    "meta"        => [
                        "errors" => $messages->errors(),
                    ],
                ]);
            }

            return $this->setResponse([
                "code"       => 200,
                "title"      => "Successfully create message.",
                "parameters" => $messages,
            ]);
        
    }

    public function update($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }
        if (!isset($data['user_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "user id is not set.",
            ]);
        }
        
        if (!isset($data['location_lat'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "location_lat is not set.",
            ]);
        }
        
        if (!isset($data['location_long'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "location_long is not set.",
            ]);
        }

        if (!isset($data['sender'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "sender is not set.",
            ]);
        }
        
        if (!isset($data['reciever'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "reciever is not set.",
            ]);
        }
        if (!isset($data['messages'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "messages is not set.",
            ]);
        }
        if (!isset($data['approval_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "approval_id is not set.",
            ]);
        }
        if (!isset($data['image'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "image is not set.",
            ]);
        }else{
            $data['image_url']=null;      
                request()->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg|max:10000',
        
                ]);
                $imageName = time().'.'.request()->image->getClientOriginalExtension();
                define('UPLOAD_DIR', 'storage/images/');
                $file =  request()->image->move(UPLOAD_DIR,$imageName);
                $url= asset($file);
                $data['image_url'] = $url;        
        }

        $messages = $this->messages->find($data['id']);
        if($messages==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }

        $messages->save($data);
        if (!$messages->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $messages->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a messages.",
            "meta"        => [
                "status" => $messages,
            ]
        ]);
            
        
    }
    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $messages = $this->messages->find($data['id']);
        if($messages==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "messages not found.",
            ]);
        }
        
        if (!$messages->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $messages->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a messages.",
            "meta"        => [
                "status" => $messages,
            ]
        ]);
            
        
    }


}
