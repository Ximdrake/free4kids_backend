<?php

namespace App\Data\Repositories;

use App\Data\Models\ReportsModel;
use App\Data\Repositories\BaseRepository;
use App\Reports;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class ReportsRepository extends BaseRepository
{

    protected $reports;

    public function __construct(ReportsModel $reports) 
    {
        $this->reports = $reports;
    }

    public function fetchReports($data = [])
    {
        $meta_index = "reports";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "reports";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->reports);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function create($data = [])
    {
        // data validation
        
            if (!isset($data['user_id'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "user_id is not set.",
                ]);
            }
         
            if (!isset($data['reports'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "reports is not set.",
                ]);
            }

    
            $reports = $this->reports->init($this->reports->pullFillable($data));
            $reports->save($data);

            if (!$reports->save($data)) {
                return $this->setResponse([
                    "code"        => 500,
                    "title"       => "Data Validation Error.",
                    "description" => "An error was detected on one of the inputted data.",
                    "meta"        => [
                        "errors" => $reports->errors(),
                    ],
                ]);
            }

            return $this->setResponse([
                "code"       => 200,
                "title"      => "Successfully creaete reports.",
                "parameters" => $reports,
            ]);
        
    }

    public function update($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        if (!isset($data['user_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "user_id is not set.",
            ]);
        }
     
        if (!isset($data['reports'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "reports is not set.",
            ]);
        }


        $reports = $this->reports->find($data['id']);
        if($reports==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }

        $reports->save($data);
        if (!$reports->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $reports->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a reports.",
            "meta"        => [
                "status" => $reports,
            ]
        ]);
            
        
    }

    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $reports = $this->reports->find($data['id']);
        if($reports==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$reports->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $reports->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a reports.",
            "meta"        => [
                "status" => $reports,
            ]
        ]);
            
        
    }


}
