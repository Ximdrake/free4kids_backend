<?php

namespace App\Data\Repositories;

use App\Data\Models\ReservationModel;
use App\Data\Repositories\BaseRepository;
use App\Reservations;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class ReservationRepository extends BaseRepository
{

    protected $reservation;

    public function __construct(ReservationModel $reservation) 
    {
        $this->reservation = $reservation;
    }

    public function fetchReservation($data = [])
    {
        $meta_index = "reservation";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "reservation";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->reservation);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function create($data = [])
    {
        // data validation
        
          
            if (!isset($data['product_id'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "product_id is not set.",
                ]);
            }
            
            if (!isset($data['undo'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "undo is not set.",
                ]);
            }

            if (!isset($data['redo'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "redo is not set.",
                ]);
            }
            if (!isset($data['user_id'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "user_id is not set.",
                ]);
            }

    
            $reservation = $this->reservation->init($this->reservation->pullFillable($data));
            $reservation->save($data);

            if (!$reservation->save($data)) {
                return $this->setResponse([
                    "code"        => 500,
                    "title"       => "Data Validation Error.",
                    "description" => "An error was detected on one of the inputted data.",
                    "meta"        => [
                        "errors" => $reservation->errors(),
                    ],
                ]);
            }

            return $this->setResponse([
                "code"       => 200,
                "title"      => "Successfully reservation.",
                "parameters" => $reservation,
            ]);
        
    }

    public function update($data = [])
    {
        if(!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }
        if(!isset($data['product_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "product_id is not set.",
            ]);
        }
        
        if (!isset($data['undo'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "undo is not set.",
            ]);
        }

        if (!isset($data['redo'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "redo is not set.",
            ]);
        }
        if (!isset($data['user_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "user_id is not set.",
            ]);
        }
        $reservation = $this->reservation->find($data['id']);
        $reservation->save($data);
        if (!$reservation->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $reservation->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a reservation.",
            "meta"        => [
                "status" => $reservation,
            ]
        ]);
            
        
    }

    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $reservation = $this->reservation->find($data['id']);
        if($reservation==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$reservation->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $reservation->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a reservation.",
            "meta"        => [
                "status" => $reservation,
            ]
        ]);
            
        
    }


}
