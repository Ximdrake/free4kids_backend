<?php

namespace App\Data\Repositories;

use App\Data\Models\ProductCategory;
use App\Data\Repositories\BaseRepository;
use App\ProductsCategory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class productCategoryRepository extends BaseRepository
{

    protected $product_category;

    public function __construct(ProductCategory $product_category) 
    {
        $this->product_category = $product_category;
    }

    public function fetchProductCategory($data = [])
    {
        $meta_index = "product_category";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "product_category";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->product_category);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function create($data = [])
    {
        // data validation
        
            if (!isset($data['name'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "name is not set.",
                ]);
            }
            if (!isset($data['user_id'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "user_id is not set.",
                ]);
            }

    
            $product_category = $this->product_category->init($this->product_category->pullFillable($data));
            $product_category->save($data);

            if (!$product_category->save($data)) {
                return $this->setResponse([
                    "code"        => 500,
                    "title"       => "Data Validation Error.",
                    "description" => "An error was detected on one of the inputted data.",
                    "meta"        => [
                        "errors" => $product_category->errors(),
                    ],
                ]);
            }

            return $this->setResponse([
                "code"       => 200,
                "title"      => "Successfully create message.",
                "parameters" => $product_category,
            ]);
        
    }

    public function update($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }
        $product_category = $this->product_category->find($data['id']);
        if($product_category==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }

        if(isset($data["name"]))
        {
                $url = $product_category->product_category;
                $file_name = basename($url);
        }

        $product_category->save($data);
        if (!$product_category->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $product_category->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a product.",
            "meta"        => [
                "status" => $product_category,
            ]
        ]);
            
        
    }
    
    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $product_category = $this->product_category->find($data['id']);
        if($product_category==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$product_category->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $product_category->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a product category.",
            "meta"        => [
                "status" => $product_category,
            ]
        ]);
            
        
    }


}
