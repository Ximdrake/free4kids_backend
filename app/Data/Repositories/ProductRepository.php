<?php

namespace App\Data\Repositories;

use App\Data\Models\Product;
use App\Data\Repositories\BaseRepository;
use App\Products;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class ProductRepository extends BaseRepository
{

    protected $product;

    public function __construct(Product $product) 
    {
        $this->product = $product;
    }

    public function fetchProduct($data = [])
    {
        $meta_index = "product";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "product";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->product);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function productReviews($data = [])
    {
        $meta_index = "product";
        $parameters = [];
        $count = 0;
        $data['relations'] = ["reviews"];
        if (isset($data['product_id']) &&
            is_numeric($data['product_id'])) {

            $meta_index = "product";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['product_id'],
                ],
            ];

            $parameters['product_id'] = $data['product_id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->product);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    
    public function create($data = [])
    {
        // data validation
        if (!isset($data['user_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "user id is not set.",
            ]);
        }

        if (!isset($data['category_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "category id is not set.",
            ]);
        }

        
            if (!isset($data['name'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "name is not set.",
                ]);
            }

            if (!isset($data['image'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "image is not set.",
                ]);
            }else{
                $data['image_url']=null;      
                    request()->validate([
                        'image' => 'required|image|mimes:jpeg,png,jpg|max:10000',
            
                    ]);
                    $imageName = time().'.'.request()->image->getClientOriginalExtension();
                    define('UPLOAD_DIR', 'storage/images/');
                    $file =  request()->image->move(UPLOAD_DIR,$imageName);
                    $url= asset($file);
                    $data['image_url'] = $url;        
            }
       
            $product = $this->product->init($this->product->pullFillable($data));
            $product->save($data);

            if (!$product->save($data)) {
                return $this->setResponse([
                    "code"        => 500,
                    "title"       => "Data Validation Error.",
                    "description" => "An error was detected on one of the inputted data.",
                    "meta"        => [
                        "errors" => $product->errors(),
                    ],
                ]);
            }

            return $this->setResponse([
                "code"       => 200,
                "title"      => "Successfully create product.",
                "parameters" => $product,
            ]);
        
    }

    public function update($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        if (!isset($data['user_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "user id is not set.",
            ]);
        }

        if (!isset($data['category_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "category id is not set.",
            ]);
        }

        
            if (!isset($data['name'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "name is not set.",
                ]);
            }

            if (!isset($data['image'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "image is not set.",
                ]);
            }else{
                $data['image_url']=null;      
                    request()->validate([
                        'image' => 'required|image|mimes:jpeg,png,jpg|max:10000',
            
                    ]);
                    $imageName = time().'.'.request()->image->getClientOriginalExtension();
                    define('UPLOAD_DIR', 'storage/images/');
                    $file =  request()->image->move(UPLOAD_DIR,$imageName);
                    $url= asset($file);
                    $data['image_url'] = $url;        
            }
            
        $product = $this->product->find(2);
        
        if($product==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }
    
       

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a product.",
            "meta"        => [
                "status" => $product,
            ]
        ]);
            
        
    }
    
    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $product = $this->product->find($data['id']);
        if($product==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$product->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $product->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a product.",
            "meta"        => [
                "status" => $product,
            ]
        ]);
            
        
    }


}
