<?php

namespace App\Data\Repositories;

use App\Data\Models\Users;
use App\Data\Models\ApprovalModel;
use App\Data\Models\Location;
use App\Data\Models\Product;
use App\Data\Models\ReservationModel;
use App\Data\Models\ProductCategory;
use App\Data\Models\ReportsModel;
use App\Data\Repositories\BaseRepository;
use App\User;
use App\Reports;
use App\Reservations;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UsersRepository extends BaseRepository
{

    protected $users;

    public function __construct(
        Users $users , Location $location, 
        ReportsModel $reports, 
        Product $products
    ) {
        $this->users = $users;
        $this->location = $location;
        $this->reports = $reports;
        $this->product = $products;
    }

    public function fetchUser($data = [])
    {
        $meta_index = "users";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "users";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->users);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->users->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function location($data = [])
    {
        $meta_index = "users";
        $parameters = [];
        $count = 0;
        $data['relations'] = ["location"];
        if (isset($data['user_id']) &&
            is_numeric($data['user_id'])) {

            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['user_id'],
                ],
            ];

            $parameters['user_id'] = $data['user_id'];

        }

        $count_data = $data;

        $result = $this->fetchGeneric($data, $this->users);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->users->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function reports($data = [])
    {
        $meta_index = "users";
        $parameters = [];
        $count = 0;
        $data['relations'] = ["reports"];
        if (isset($data['user_id']) &&
            is_numeric($data['user_id'])) {

            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['user_id'],
                ],
            ];

            $parameters['user_id'] = $data['user_id'];

        }

        $count_data = $data;

        $result = $this->fetchGeneric($data, $this->users);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->users->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }
    public function products($data = [])
    {

        $meta_index = "product";
        $parameters = [];
        $count = 0;
        $data['order'] = "desc";
        $data['sort'] = "created_at";
        
        if (isset($data['user_id']) &&
            is_numeric($data['user_id'])) { 
            $data['single'] = false;
            $data['where'] = [
                [
                    "target" => "user_id",
                    "operator" => "=",
                    "value" => $data['user_id'],
                ],
            ];
            $parameters['user_id'] = $data['user_id'];
        }
        // $meta_index = "users";
        // $parameters = [];
        // $count = 0;
        // $data['relations'] = ["products"];
        // $data['sort'] = 'created_at';
        // $data['order'] = 'desc';
        // if (isset($data['user_id']) &&
        //     is_numeric($data['user_id'])) { 
        //     $data['single'] = true;
        //     $data['where'] = [
        //         [
        //             "target" => "id",
        //             "operator" => "=",
        //             "value" => $data['user_id'],
        //         ],
        //     ];
        //     $parameters['user_id'] = $data['user_id'];
        // }

        $count_data = $data;

        $result = $this->fetchGeneric($data, $this->product);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->users->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    } 

    public function product_category($data = [])
    {
        $meta_index = "users";
        $pcatObj = ProductCategory::all();
        $prodObj = Product::all();
        $userObj = Users::all()->where('id',$data['user_id']);
        $result = [];
        foreach ($userObj as $key => $value) {
                    $user_id = $value->id;
                    array_push($result, "users");
                    array_push($result, $value);
                    foreach ($prodObj as $key => $value2) {
                      if($value2->user_id == $user_id)
                      {
                        $prod_id = $value2->id;
                        array_push($result, "product");
                        array_push($result, $value2);
                        foreach ($pcatObj as $key => $value3) {
                            if($value3->product_id == $prod_id)
                            {
                              array_push($result, "product category");
                              array_push($result, $value3);
                            }
                          }
                      }
                    }
            }


        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No Users are found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved users->product->category",
            "description" => "UserInfo",
            "meta" => [
                $meta_index => $result,
            ],

        ]);

    } 

    public function allProdCat($data = [])
    {
        $meta_index = "category";
        $pcatObj = ProductCategory::all();
        $prodObj = Product::all();
        $result = [];
        foreach ($pcatObj as $key => $value2) {
                        $pcat_id = $value2->id;
                        array_push($result, "category");
                        array_push($result, $value2);
                        foreach ($prodObj as $key => $value3) {
                            if($value3->category_id == $pcat_id)
                            {
                              array_push($result, "product");
                              array_push($result, $value3);
                            }
                          }
                    }

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No Users are found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved All-category->category",
            "description" => "Product&Category",
            "meta" => [
                $meta_index => $result,
            ],

        ]);

    } 

    public function reviews($data = [])
    {
        $meta_index = "users";
        $parameters = [];
        $count = 0;
        $data['relations'] = ["reviews"];
        if (isset($data['user_id']) &&
            is_numeric($data['user_id'])) {

            $meta_index = "users";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['user_id'],
                ],
            ];

            $parameters['user_id'] = $data['user_id'];

        }

        $count_data = $data;

        $result = $this->fetchGeneric($data, $this->users);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->users->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved profile reviews",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function reservation($data = [])
    {
        $meta_index = "users";
        $presObj = ReservationModel::all();
        $prodObj = Product::all();
        $userObj = Users::all()->where('id',$data['user_id']);
        $result = [];
        foreach ($userObj as $key => $value) {
                    $user_id = $value->id;
                    array_push($result, "users");
                    array_push($result, $value);
                    foreach ($prodObj as $key => $value2) {
                      if($value2->user_id == $user_id)
                      {
                        $prod_id = $value2->id;
                        array_push($result, "product");
                        array_push($result, $value2);
                        foreach ($presObj as $key => $value3) {
                            if($value3->product_id == $prod_id)
                            {
                              array_push($result, "product reservation");
                              array_push($result, $value3);
                            }
                          }
                      }
                    }
            }


        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No Users are found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved users->product->category",
            "description" => "UserInfo",
            "meta" => [
                $meta_index => $result,
            ],

        ]);

    } 

    public function approval($data = [])
    {
        $meta_index = "users";
        $presObj = ReservationModel::all();
        $approObj = ApprovalModel::all();
        $prodObj = Product::all();
        $userObj = Users::all()->where('id',$data['user_id']);
        $result = [];
        foreach ($userObj as $key => $value) {
                    $user_id = $value->id;
                    array_push($result, "users");
                    array_push($result, $value);
                    foreach ($prodObj as $key => $value2) {
                      if($value2->user_id == $user_id)
                      {
                        $prod_id = $value2->id;
                        array_push($result, "product");
                        array_push($result, $value2);
                        foreach ($presObj as $key => $value3) {
                            if($value3->product_id == $prod_id)
                            {
                             $reserved_id = $value3->id;    
                              array_push($result, "product reservation");
                              array_push($result, $value3);

                              foreach ($approObj as $key => $value4) {
                                if($value4->reserved_id == $reserved_id)
                                {
                                  array_push($result, "approval");
                                  array_push($result, $value4);
                                }
                              }
                            }
                          }
                      }
                    }
            }
        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No Users are found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved users->product->category",
            "description" => "UserInfo",
            "meta" => [
                $meta_index => $result,
            ],

        ]);

    } 

    public function messages($data = [])
    {
        $meta_index = "users";
        $parameters = [];
        $count = 0;
        $data['relations'] = ["messages"];
        if (isset($data['user_id']) &&
            is_numeric($data['user_id'])) {

            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['user_id'],
                ],
            ];

            $parameters['user_id'] = $data['user_id'];

        }

        $count_data = $data;

        $result = $this->fetchGeneric($data, $this->users);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->users->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    } 

    public function create($data = [])
    {
        // data validation
        
            if (!isset($data['name'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "Name is not set.",
                ]);
            }

            if (!isset($data['email'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "email is not set.",
                ]);
            }
            if (!isset($data['user_name'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "username is not set.",
                ]);
            }
            if (!isset($data['image'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "image is not set.",
                ]);
            }else{
                $data['image_url']=null;      
                    request()->validate([
                        'image' => 'required|image|mimes:jpeg,png,jpg|max:10000',
            
                    ]);
                    $imageName = time().'.'.request()->image->getClientOriginalExtension();
                    define('UPLOAD_DIR', 'storage/images/');
                    $file =  request()->image->move(UPLOAD_DIR,$imageName);
                    $url= asset($file);
                    $data['image_url'] = $url;        
            }
            $data['password']= Hash::make($data['password']);

       
            $user = $this->users->init($this->users->pullFillable($data));
            $user->save($data);

            if (!$user->save($data)) {
                return $this->setResponse([
                    "code"        => 500,
                    "title"       => "Data Validation Error.",
                    "description" => "An error was detected on one of the inputted data.",
                    "meta"        => [
                        "errors" => $user->errors(),
                    ],
                ]);
            }

            return $this->setResponse([
                "code"       => 200,
                "title"      => "Successfully defined a user.",
                "parameters" => $user,
            ]);
        
    }

    public function update($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }
        $user = $this->users->find($data['id']);
        if($user==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }
    
        if(isset($data["image"])){   
                $url = $user->image_url;
                $file_name = basename($url);
                Storage::delete('images/' . $file_name);
                define('UPLOAD_DIR', 'storage/images/');
                $file = request()->image->move(UPLOAD_DIR, $data['image']);
                $url = asset($file);
                $data['image_url'] = $url;
        }

        $user->save($data);
        if (!$user->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $user->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a User.",
            "meta"        => [
                "status" => $user,
            ]
        ]);
            
        
    }
    
    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $user = $this->users->find($data['id']);
        if($user==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "User not found.",
            ]);
        }
        
        if (!$user->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $user->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a User.",
            "meta"        => [
                "status" => $user,
            ]
        ]);
            
        
    }


}
