<?php

namespace App\Data\Repositories;

use App\Data\Models\Location;
use App\Data\Repositories\BaseRepository;
use App\Locations;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class LocationRepository extends BaseRepository
{

    protected $location;

    public function __construct(Location $location) 
    {
        $this->location = $location;
    }

    public function fetchLocation($data = [])
    {
        $meta_index = "location";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "location";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->location);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function create($data = [])
    {
        // data validation
        
            if (!isset($data['street_address1'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "street_address1 is not set.",
                ]);
            }
            if (!isset($data['street_address2'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "street_address2 is not set.",
                ]);
            }
            if (!isset($data['city'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "city is not set.",
                ]);
            }
            if (!isset($data['state'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "state is not set.",
                ]);
            }
        
            if (!isset($data['postal_code'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "postal_code is not set.",
                ]);
            }
            if (!isset($data['latitude'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "latitude is not set.",
                ]);
            }
            if (!isset($data['longitude'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "longitude is not set.",
                ]);
            }

    
            $location = $this->location->init($this->location->pullFillable($data));
            $location->save($data);

            if (!$location->save($data)) {
                return $this->setResponse([
                    "code"        => 500,
                    "title"       => "Data Validation Error.",
                    "description" => "An error was detected on one of the inputted data.",
                    "meta"        => [
                        "errors" => $location->errors(),
                    ],
                ]);
            }

            return $this->setResponse([
                "code"       => 200,
                "title"      => "Successfully Location.",
                "parameters" => $location,
            ]);
        
    }

    public function update($data = [])
    {
        if (!isset($data['street_address1'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "street_address1 is not set.",
            ]);
        }
        if (!isset($data['street_address2'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "street_address2 is not set.",
            ]);
        }
        if (!isset($data['city'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "city is not set.",
            ]);
        }
        if (!isset($data['state'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "state is not set.",
            ]);
        }
    
        if (!isset($data['postal_code'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "postal_code is not set.",
            ]);
        }
        if (!isset($data['latitude'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "latitude is not set.",
            ]);
        }
        if (!isset($data['longitude'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "longitude is not set.",
            ]);
        }
        if (!isset($data['user_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "user_id is not set.",
            ]);
        }
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

            $location = $this->location->find($data['id']);
        if($location==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }

        $location->save($data);
        if (!$location->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $location->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a location.",
            "meta"        => [
                "status" => $location,
            ]
        ]);
            
        
    }

    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $location = $this->location->find($data['id']);
        if($location==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$location->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $location->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a location.",
            "meta"        => [
                "status" => $location,
            ]
        ]);
            
        
    }


}
