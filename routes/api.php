<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group([
    "prefix" => "v1",
    "middleware" => "auth:api",
], function () {

    Route::group([
        "prefix" => "users",
    ], function () {

        Route::get("/", "UserController@index");
        Route::post("create", "UserController@create");
        Route::post("update", "UserController@update");
        Route::post('delete', 'UserController@delete');
        Route::post('reviews', 'UserController@reviews');
        Route::post('location', 'UserController@location');
        Route::post('products', 'UserController@products');
        Route::post('reports', 'UserController@reports');
        Route::post('product_category', 'UserController@product_category');
        Route::post('reservation', 'UserController@reservation');
        Route::post('approval', 'UserController@approval');
        Route::post('messages', 'UserController@messages');
        Route::get('allProdCat', 'UserController@allProdCat');
        Route::post('productReviews', 'UserController@productReviews');
    });

    
    Route::group([
        "prefix" => "messages",
    ], function () {

        Route::get("/", "MessagesController@index");
        Route::post("create", "MessagesController@create");
        Route::post("update", "MessagesController@update");
        Route::post('delete', 'MessagesController@delete');

    });


    Route::group([
        "prefix" => "products",
    ], function () {

        Route::get("/", "ProductController@index");
        Route::post("create", "ProductController@create");
        Route::post("update", "ProductController@update");
        Route::post('delete', 'ProductController@delete');
        Route::get('productReviews', 'ProductController@productReviews');

    });

    Route::group([
        "prefix" => "category",
    ], function () {

        Route::get("/", "ProductCategoryController@index");
        Route::post("create", "ProductCategoryController@create");
        Route::post("update", "ProductCategoryController@update");
        Route::post('delete', 'ProductCategoryController@delete');
        Route::post('productCategory', 'ProductCategoryController@productCategory');

    });

    Route::group([
        "prefix" => "locations",
    ], function () {

        Route::get("/", "LocationController@index");
        Route::post("create", "LocationController@create");
        Route::post("update", "LocationController@update");
        Route::post('delete', 'LocationController@delete');

    });

    Route::group([
        "prefix" => "reports",
    ], function () {

        Route::get("/", "ReportsController@index");
        Route::post("create", "ReportsController@create");
        Route::post("update", "ReportsController@update");
        Route::post('delete', 'ReportsController@delete');

    });

    Route::group([
        "prefix" => "reservation",
    ], function () {

        Route::get("/", "ReservationController@index");
        Route::post("create", "ReservationController@create");
        Route::post("update", "ReservationController@update");
        Route::post('delete', 'ReservationController@delete');

    });

    
    Route::group([
        "prefix" => "approval",
    ], function () {

        Route::get("/", "ApprovalController@index");
        Route::post("create", "ApprovalController@create");
        Route::post("update", "ApprovalController@update");
        Route::post('delete', 'ApprovalController@delete');

    });

    Route::group([
        "prefix" => "reviews",
    ], function () {

        Route::get("/", "ReviewsController@index");
        Route::post("create", "ReviewsController@create");
        Route::post("update", "ReviewsController@update");
        Route::post('delete', 'ReviewsController@delete');

    });

  
});

